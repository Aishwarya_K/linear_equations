import numpy as np
import re

def solution(eqn_list):
   
    #extract all the coefficients and constants
    coeff=[]
    for eqn in eqn_list:
        z= re.findall(r'[\d\-\+]+',eqn)
        coeff.append(z)
    
    #build matrices from coeff
    a=[]
    b=[]
    for i in range(len(coeff)):
        a.append(coeff[i][:-1])
        b.append(coeff[i][-1])
    a = [[int(x) for x in n] for n in a]  
    b = [int(x) for x in b]
    
    #apply linear algorithm & get the output values
    res = list(np.linalg.solve(np.array(a),np.array(b)))

    #print the output
    output=[]
    var=re.findall('[a-zA-Z]',eqn_list[0])
    for i in range(len(var)):
        for j in range(len(res)):
            if i==j:
                result=var[i]+" : "+ str(np.round(res[j],3))
                output.append(result)
    return output
                

