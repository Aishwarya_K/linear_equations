import re
def validate_eqn(eqn_list):
    var=[]
    msg="Equations are valid"
    
    #extract all the variables from the eqns
    for eqn in eqn_list:
        var.append(re.findall('[a-zA-Z]',eqn))
    
    #check if the number of equations are sufficient
    if(len(eqn_list)!=len(max(var))):
        return "Insufficient number of equations!"
    
    #check if the variables are correct
    flag = 0
    for sub_list in range(len(var)-1):
        if(all(x in var[sub_list] for x in var[sub_list+1])):
            flag = 1
    if (flag==0) :
        return "Variables didn't match"
    return 0
        

