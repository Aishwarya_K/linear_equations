from fastapi import FastAPI
import uvicorn
from pydantic import BaseModel
from solve import *
from validation import *

app= FastAPI()

class eqn_input(BaseModel):
    eqn : list

@app.post('/linear_eqn/')
async def solve_eqn(item: eqn_input):
    result = validate_eqn(item.eqn)
    if result==0:
        return solution(item.eqn)
    return validate_eqn(item.eqn)



